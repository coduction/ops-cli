# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.3] - 2019-02-23

### Fixed

- Verification that changelog version does not exist as a Git version yet

## [0.1.2] - 2019-02-10

### Added

- Link documentation of commands in README.md

## [0.1.1] - 2019-02-10

### Added

- Automatic release to NPM via GitLab CI
- Automatic versioning via `ops-cli version create`
- Improve `README.md`

### Fixed

- Validation of versions in `package.json` and `package-lock.json`

## [0.1.0] - 2019-02-10

### Added

- CI pipeline with GitLab
- [Prettier](https://prettier.io) for code formatting
- [ESLint](https://eslint.org) for code linting
- [pre-commit](https://www.npmjs.com/package/pre-commit) to enforce formatting and linting
- `ops-cli version create` command to create and push a git tag automatically based on a changelog
- `ops-cli version check` command to verify the version configuration
- Documentation for `ops-cli version [subcommand]`
