# @coduction/ops-cli

A command line tool to automate common operations tasks.

## Installation

```shell
$ npm i -g @coduction/ops-cli
```

## Usage

Display help:

```shell
$ ops-cli
Usage: ops-cli [options] [command]

A command line tool to automate common operations tasks.

Options:
  -V, --version  output the version number
  -h, --help     output usage information

Commands:
  version        manage versions in a git repository
  help [cmd]     display help for [cmd]
```

Command documentation and usage examples:

- [`ops-cli version`](https://gitlab.com/coduction/ops-cli/blob/master/docs/version.md)

## Developing

After modifying the program, you can test the changes locally by making the CLI available in your `PATH`. To do this run:

```shell
$ npm link
```

## License

This project is licensed under the terms of the [MIT license](https://mit-license.org).
