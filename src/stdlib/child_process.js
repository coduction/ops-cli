const cp = require('child_process');
const { promisify } = require('util');

exports.execFile = promisify(cp.execFile);
