const fs = require('fs');
const { promisify } = require('util');
const quit = require('../util/quit');
const errors = require('../util/errors');

// default fs API with promises
exports.exists = promisify(fs.exists);
exports.readFile = promisify(fs.readFile);
exports.stat = promisify(fs.stat);

// custom fs API
exports.readJson = async (file, ...args) => {
  const jsonBuffer = await exports.readFile(file, ...args);
  try {
    return JSON.parse(jsonBuffer.toString());
  } catch (err) {
    return quit(errors.PARSING_JSON_FAILED({ file }));
  }
};
