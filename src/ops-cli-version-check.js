const program = require('commander');
const errors = require('./util/errors');
const logger = require('./util/logger');
const quit = require('./misc/version');
const version = require('./misc/version');

program.description('Check version configuration.').parse(process.argv);

(async function main() {
  await version.checkVersionAlignment();

  logger.info('Versioning check successful.');
  logger.info('All versions are synchronized.');

  return process.exit(0);
})().catch(err =>
  quit(errors.UNKNOWN_ERROR({ action: 'checking the version' }), 1, err)
);
