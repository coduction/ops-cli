const program = require('commander');

program
  .description('Manage versions in a git repository.')
  .command('check', 'check version configuration')
  .command('create', 'create a new git tag')
  .parse(process.argv);
