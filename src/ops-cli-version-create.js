#!/usr/bin/env node

const program = require('commander');
const axios = require('axios');
const git = require('./bin/git');
const logger = require('./util/logger');
const quit = require('./util/quit');
const errors = require('./util/errors');
const version = require('./misc/version');

(async function main() {
  program
    .description('Create a new git tag.')
    .option('-a,--author <author>', 'full name used for git commits')
    .option('-e,--email <email>', 'email address used for git commits')
    .option('-u,--username <username>', 'git server username')
    .option('-t,--token <token>', 'git API token')
    .option('-r,--remote <remote>', 'git server remote repository url')
    .option(
      '-p,--project-id [projectId]',
      'gitlab project ID to create release'
    )
    .option(
      '-m,--message [message]',
      'git tag message used in annotated tag',
      'For changes, see the CHANGELOG.md.'
    )
    .parse(process.argv);

  const [
    changelogVersion,
    releaseInformation
  ] = await version.checkVersionAlignment();

  logger.info(`Creating new tag: ${changelogVersion}`);

  // configure git commit author
  if (!program.author) {
    return quit(errors.OPTION_REQUIRED({ option: '-a,--author <author>' }));
  }
  await git.configUserName(program.author);

  // configure git commit email
  if (!program.email) {
    return quit(errors.OPTION_REQUIRED({ option: '-e,--email <email>' }));
  }
  await git.configEmail(program.email);

  // configure git remote
  if (!program.username) {
    return quit(errors.OPTION_REQUIRED({ option: '-u,--username <username>' }));
  }
  if (!program.token) {
    return quit(errors.OPTION_REQUIRED({ option: '-t,--token <token>' }));
  }
  if (!program.remote) {
    return quit(errors.OPTION_REQUIRED({ option: '-r,--remote <remote>' }));
  }
  const { username, token } = program;
  const remote = program.remote.split('://').join(`://${username}:${token}@`);
  await git.setOriginRemoteUrl(remote);

  await git.createAnnotatedTag(changelogVersion, program.message);
  await git.pushTags();

  logger.info(`Successfully created tag: ${changelogVersion}`);

  // create gitlab release
  if (program.projectId) {
    const { projectId } = program;
    const url = `https://gitlab.com/api/v4/projects/${projectId}/releases`;
    const payload = {
      name: changelogVersion,
      tag_name: changelogVersion,
      description: releaseInformation
    };
    const config = {
      headers: {
        'PRIVATE-TOKEN': program.token
      }
    };
    await axios.post(url, payload, config);
    logger.info(`Successfully created release: ${changelogVersion}`);
  }

  return process.exit(0);
})().catch(err => {
  quit(errors.UNKNOWN_ERROR({ action: 'creating a version' }), 1, err);
});
