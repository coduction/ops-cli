#!/usr/bin/env node

const program = require('commander');
const packageJson = require('../package.json');

program
  .version(packageJson.version)
  .description(packageJson.description)
  .command('version', 'manage versions in a git repository')
  .parse(process.argv);
