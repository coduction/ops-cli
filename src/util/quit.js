const logger = require('./logger');

module.exports = (
  message = 'An unknown error occurred.',
  exitCode = 1,
  error
) => {
  const sentences = message.split(/\. /g).filter(sentence => !!sentence);
  sentences.forEach((sentence, i) => {
    const isLastItem = i === sentences.length - 1;
    logger.error(isLastItem ? sentence : `${sentence}.`);
  });

  if (error) {
    const { log } = console;
    log(error.stack || error.message);
  }
  return process.exit(exitCode);
};
