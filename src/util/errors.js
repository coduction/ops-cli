// general errors
exports.UNKNOWN_ERROR = ({ action }) =>
  `An unknown error occurred while ${action}. Please create an issue at https://gitlab.com/coduction/ops-cli.`;
exports.PARSING_JSON_FAILED = ({ file }) =>
  `The ${file} could not be parsed. Does it contain valid JSON?`;
exports.OS_NOT_SUPPORTED = ({ os }) =>
  `Currently the OS platform ${os} is not supported.`;
exports.OPTION_REQUIRED = ({ option }) => `The option ${option} is required.`;

// file errors
exports.FILE_NOT_FOUND = ({ file }) =>
  `Could not find ${file}. Please navigate to the correct location and try again.`;
exports.FILE_EXPECTED = ({ file }) =>
  `Expected ${file} to be a file. Is it a directory?`;

// miscellaneous errors
exports.GIT_REPO_NOT_FOUND = () =>
  'The folder does not seem to contain a valid git repository. Please navigate to a git repository and try again.';
exports.BINARY_NOT_FOUND = ({ binary }) =>
  `Could not find binary ${binary}. Is it installed?`;
exports.VERSION_EXISTS = ({ version }) =>
  `Version ${version} in CHANGELOG.md already exists. This commit will not trigger a release.`;
exports.VERSION_DOES_NOT_MATCH_CHANGELOG = ({
  file,
  version,
  changelogVersion
}) =>
  `Version ${version} in ${file} does not match version ${changelogVersion} in changelog.`;
exports.PARSING_CHANGELOG_FAILED = ({ file }) =>
  `Parsing the changelog failed. The ${file} has to be compliant to https://keepachangelog.com/en/1.0.0/.`;
