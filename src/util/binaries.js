const { platform } = require('os');
const { execFile } = require('../stdlib/child_process');
const quit = require('../util/quit');
const errors = require('../util/errors');

exports.getBinary = async binary => {
  const os = platform();
  let pathfinder = '';

  if (os === 'win32') pathfinder = 'where';
  if (os === 'linux') pathfinder = 'which';

  if (pathfinder) {
    try {
      const streams = await execFile(pathfinder, [binary]);
      return streams.stdout
        .split('\n')
        .shift()
        .replace(/(\r|\n)/g, '');
    } catch (err) {
      return quit(errors.BINARY_NOT_FOUND({ binary: 'git' }));
    }
  }

  if (os === 'linux') {
    return '/bin/git';
  }

  return quit(errors.OS_NOT_SUPPORTED({ os }));
};

exports.loadBinary = binary => async args => {
  const binFile = await exports.getBinary(binary);
  const streams = await execFile(binFile, args);
  return streams.stdout;
};
