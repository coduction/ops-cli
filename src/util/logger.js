const { createLogger, transports, format } = require('winston');

const formatText = info => {
  const { level, message, ...args } = info;
  const rest = Object.keys(args).length ? JSON.stringify(args, '', '') : '';
  return `${level}: ${message} ${rest}`;
};

module.exports = createLogger({
  transports: [
    new transports.Console({
      level: 'info',
      name: 'console',
      format: format.combine(
        format.colorize(),
        format.align(),
        format.printf(formatText)
      )
    })
  ]
});
