const path = require('path');
const { readFile, readJson, exists, stat } = require('../stdlib/fs');
const git = require('../bin/git');
const quit = require('../util/quit');
const logger = require('../util/logger');
const errors = require('../util/errors');

// parse latest version from CHANGELOG.md
exports.getChangelogInformation = async () => {
  const changelogFile = 'CHANGELOG.md';
  const changelogPath = path.join(process.cwd(), changelogFile);
  const changelogExists = await exists(changelogPath);

  if (!changelogExists) {
    return quit(errors.FILE_NOT_FOUND({ file: changelogFile }));
  }

  const changelogStats = await stat(changelogPath);
  if (!changelogStats.isFile()) {
    return quit(errors.FILE_EXPECTED({ file: changelogFile }));
  }

  const changelogBuffer = await readFile(changelogPath);
  const changelog = changelogBuffer.toString();

  const changelogVersionLines = changelog.match(/\r?\n##\s\[.*\].*\r?\n/g);

  if (!changelogVersionLines || !changelogVersionLines.length) {
    return quit(errors.PARSING_CHANGELOG_FAILED({ file: changelogFile }));
  }

  const changelogChunk = changelogVersionLines[0].split(' ')[1];
  if (!changelogChunk) {
    return quit(errors.PARSING_CHANGELOG_FAILED({ file: changelogFile }));
  }

  const changelogVersion = changelogChunk.replace(/(\[|\]|\r?\n)/g, '');

  const changelogLines = changelog.split(/\r?\n/g);
  const findChangelogVersionLine = line => i =>
    i === line.replace(/\r?\n/g, '');
  const currentVersionLine = changelogLines.findIndex(
    findChangelogVersionLine(changelogVersionLines[0])
  );
  const lastLine = changelogLines.length - 1;
  const endLine =
    changelogVersionLines.length === 1
      ? lastLine
      : changelogLines.findIndex(
          findChangelogVersionLine(changelogVersionLines[1])
        );

  const releaseInformation = changelogLines
    .slice(currentVersionLine, endLine)
    .join('\r\n')
    .replace(/(\s*\r?\n)*$/g, '');

  return [changelogVersion, releaseInformation];
};

// read version from package*.json
exports.getPackageVersion = async file => {
  const packageJsonExsists = await exists(file);
  if (!packageJsonExsists) {
    return '';
  }
  const packageJsonStats = await stat(file);
  if (!packageJsonStats.isFile()) {
    logger.warn(errors.FILE_EXPECTED({ file }));
    return '';
  }
  const packageJson = await readJson(path.join(process.cwd(), file));
  return packageJson.version || '';
};

// check if versions are aligned
exports.checkVersionAlignment = async () => {
  // run file operations in parallel
  const latestVersionPromise = git.getLatestVersion();
  const changelogInformationPromise = exports.getChangelogInformation();
  const packageVersionPromise = exports.getPackageVersion('package.json');
  const packageLockVersionPromise = exports.getPackageVersion(
    'package-lock.json'
  );

  // wait for all file operations
  const [
    changelogVersion,
    releaseInformation
  ] = await changelogInformationPromise;
  const packageVersion = await packageVersionPromise;
  const packageLockVersion = await packageLockVersionPromise;
  const latestVersion = await latestVersionPromise;

  // check if version already exists
  if (changelogVersion === latestVersion) {
    return quit(errors.VERSION_EXISTS({ version: changelogVersion }));
  }

  // allow unreleased, Unrelased, and UNRELEASED
  if (/^((U|u)nreleased|UNRELEASED)$/.test(changelogVersion)) {
    logger.info(`Version in changelog is ${changelogVersion}.`);
    logger.info('A commit will not create a tag.');
    return process.exit(0);
  }

  if (packageVersion && packageVersion !== changelogVersion) {
    return quit(
      errors.VERSION_DOES_NOT_MATCH_CHANGELOG({
        version: packageVersion,
        file: 'package.json',
        changelogVersion
      })
    );
  }

  if (packageLockVersion && packageLockVersion !== changelogVersion) {
    return quit(
      errors.VERSION_DOES_NOT_MATCH_CHANGELOG({
        version: packageLockVersion,
        file: 'package-lock.json',
        changelogVersion
      })
    );
  }

  return [changelogVersion, releaseInformation];
};
