const { loadBinary } = require('../util/binaries');
const quit = require('../util/quit');
const errors = require('../util/errors');

const git = loadBinary('git');

exports.getLatestVersion = async () => {
  try {
    const latestVersion = await git(['describe', '--abbrev=0', '--tags']);
    return latestVersion.replace(/(\r|\n)*/g, '');
  } catch (err) {
    if (err && err.stderr) {
      if (err.stderr.match(/no names found/i)) {
        return '';
      }
      if (err.stderr.match(/not a git repo/i)) {
        return quit(errors.GIT_REPO_NOT_FOUND());
      }
    }
    return quit(errors.UNKNOWN_ERROR({ action: 'calling git' }));
  }
};

exports.configUserName = async userName => {
  await git(['config', 'user.name', userName]);
};

exports.configEmail = async email => {
  await git(['config', 'user.email', email]);
};

exports.setOriginRemoteUrl = async remote => {
  await git(['remote', 'set-url', 'origin', remote]);
};

exports.createAnnotatedTag = async (version, message) => {
  await git(['tag', '-a', version, '-m', message]);
};

exports.pushTags = async () => {
  await git(['push', '--tags']);
};
