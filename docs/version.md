# ops-cli version

Manage versions in a Git repository.

## Dependencies

This script requires the following binaries to be available in your path:

- `git`

## Examples

You can verify the correct version configuration before merging:

```shell
$ ops-cli version check
```

Create a new [git](https://git-scm.com/) tag and push it to a Git server, such as [GitLab](https://gitlab.com) or [GitHub](https://github.com). This is done, based on a `CHANGELOG.md`, that is compliant to [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

```shell
$ ops-cli version create \
    --name "Nicklas Frahm" \
    --email "nicklas.frahm@gmail.com" \
    --username "nicklasfrahm" \
    --password "MySecretApiToken" \
    --remote "https://gitlab.com/coduction/ops-cli.git" \
    --message "For changes, see the CHANGELOG.md."
```

For further reference, consider the [.gitlab-ci.yml](../.gitlab-ci.yml) in this repository.

## Skipping a release

If not every feature or bugfix should be released, the version can be set to `[Unreleased]` in the `CHANGELOG.md` file. In this case, the check will succeed and the branch will not be released on merge.
